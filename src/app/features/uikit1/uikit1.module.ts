import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit1RoutingModule } from './uikit1-routing.module';
import { Uikit1Component } from './uikit1.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    Uikit1Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    Uikit1RoutingModule,
    FormsModule
  ]
})
export class Uikit1Module { }
