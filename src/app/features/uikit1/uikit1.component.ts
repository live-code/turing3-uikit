import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tur-uikit1',
  template: `
    <input type="text" ngModel>
    <tur-weather [city]="myCity"
                 [unit]="myUnit"
                 [data]="stats"
    ></tur-weather>
    
    <hr>
    
    <button (click)="myUnit = 'metric'">
      metric
    </button>
    <button (click)="myUnit = 'imperial'">
      imperial
    </button>
    
  `,
})
export class Uikit1Component {
  myCity: string | null = 'Torino';
  myUnit: 'metric' | 'imperial' | undefined;
  stats: any[] = [1, 2, 3];

  constructor() {
    setTimeout(() => {
      this.stats = [...this.stats, 4]
      console.log(this.stats)
    }, 1000)

  }


}
