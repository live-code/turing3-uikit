import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'tur-uikit2',
  template: `
    <p>
      uikit2 works!
    </p>
    <tur-temperature-chart 
      [values]="temps"
      type="bar"
    ></tur-temperature-chart>

    <tur-temperature-chart
      [values]="temps"
      type="pie"
    ></tur-temperature-chart>
    
    <tur-leaflet
      [coords]="coords"
      [zoom]="myZoom"></tur-leaflet>
    <button (click)="coords = [44, 12]">locaiton 1</button>
    <button (click)="coords = [45, 11]">locaiton 2</button>
    <button (click)="myZoom = myZoom + 1 ">+</button>
    <button (click)="myZoom = myZoom - 1 ">-</button>
  `,
})
export class Uikit2Component  {
  myZoom = 4;
  coords: any = [43, 13]
  temps: number[] = [22, 33, 44]

  constructor() {
    setTimeout(() => {
      this.temps = [44, 11, 2]
    }, 2000)

  }
}
