import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'uikit1', loadChildren: () => import('./features/uikit1/uikit1.module').then(m => m.Uikit1Module) },
  { path: 'uikit2', loadChildren: () => import('./features/uikit2/uikit2.module').then(m => m.Uikit2Module) },
  { path: 'uikit3', loadChildren: () => import('./features/uikit3/uikit3.module').then(m => m.Uikit3Module) },
  { path: '', redirectTo: 'uikit1', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
