import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, lastValueFrom, map, Observable, share } from 'rxjs';
export interface User {
  id: number;
  name: string;
}
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users: User[] = [];
  constructor(private http: HttpClient) { }

  getAll(fn: (res: User[]) => void) {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(fn)
  }


  getAll2(): Promise<User[]> {
      return lastValueFrom(
        this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      )
  }


  getAll3() {
     this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
       .subscribe({
         next: (res) => console.log(res),
         error: (err) => console.warn(err),
         complete: () => console.log('completed')
       })
  }

  deleteUser(id: number) {
    // ...http
    // update users
  }
}
