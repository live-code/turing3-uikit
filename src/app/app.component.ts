import { Component } from '@angular/core';
import { UsersService } from './core/users.service';
import { LanguageService } from './core/language.service';

@Component({
  selector: 'tur-root',
  template: `
    
    <button routerLink="uikit1">uikit1</button>
    <button routerLink="uikit2">uikit2</button>
    <button routerLink="uikit3">uikit3</button>
    <button (click)="lang.value = 'en'">en</button>
    <button (click)="lang.value = 'it'">it</button>
    {{lang.value}}
    <hr>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {
  constructor(
    public userService: UsersService,
    public lang: LanguageService
  ) {
  }

  async getAll() {
   this.userService.getAll2()
  }
}
