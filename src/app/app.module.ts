import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    /*{provide: MyService, useFactory: () => {
      return environment.production ? new PIppoService() : new PlutoService()
      }}*/
    /*{
      provide: APP_BASE_HREF,
      useFactory: () => environment.production ? '/site' : ''
    }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

