import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './components/weather.component';
import { LeafletComponent } from './components/leaflet.component';
import { ChartComponent } from './components/chart.component';
import { TemperatureChartComponent } from './components/temperature-chart.component';



@NgModule({
  declarations: [
    WeatherComponent,
    LeafletComponent,
    ChartComponent,
    TemperatureChartComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    WeatherComponent,
    LeafletComponent,
    ChartComponent,
    TemperatureChartComponent
  ]
})
export class SharedModule { }
