import { AfterViewInit, Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as L from 'leaflet';
import { LatLngExpression, LatLngLiteral } from 'leaflet';

@Component({
  selector: 'tur-leaflet',
  template: `
    <div #host class="map" ></div>
  `,
  styles: [`
    .map { height: 180px; background-color: red }
  `]
})
export class LeafletComponent {
  @Input() coords!: LatLngExpression;
  @Input() zoom: number = 5;
  @ViewChild('host', { static: true }) el!: ElementRef<HTMLElement>;
  map!: L.Map;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['coords']?.firstChange) {
      this.init();
    }
    if (changes['coords']) {
      this.map.flyTo(this.coords)
    }
    if (changes['zoom']) {
      this.map.setZoom(this.zoom)
    }
  }

  init(): void {
    this.map = L.map(this.el.nativeElement)
      .setView(this.coords, this.zoom);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }



}
