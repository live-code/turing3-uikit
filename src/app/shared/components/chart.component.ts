import { Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

@Component({
  selector: 'tur-chart',
  template: `
    <canvas #host width="400" height="400"></canvas>
  `,
})
export class ChartComponent {
  @ViewChild('host', { static: true }) el!: ElementRef<HTMLCanvasElement>;
  @Input() options: any;
  myChart!: Chart;

  ngOnChanges() {
    if (this.myChart) {
      this.myChart.data.datasets[0].data = this.options.data.datasets[0].data
      this.myChart.update()
    }
  }

  ngAfterViewInit(changes: SimpleChanges) {
    const ctx: any = this.el.nativeElement.getContext('2d');
    this.myChart = new Chart(ctx, this.options);
  }


}
