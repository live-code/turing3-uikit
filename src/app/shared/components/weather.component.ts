import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LanguageService } from '../../core/language.service';

@Component({
  selector: 'tur-weather',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p>weather works! {{city}}</p>
    <pre>{{meteo?.main.temp}}°</pre>
    {{data | json}} 
  `,
})
export class WeatherComponent {
  @Input() city: string | null = null;
  @Input() unit: 'metric' | 'imperial'  | undefined = 'metric';
  @Input() data: any[] = [];
  meteo: any;

  constructor(private http: HttpClient) {}

  ngOnChanges() {
    if (this.city) {
      this.http.get<any>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit || 'metric'}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo = res;
        })
    }
  }


  render() {
    console.log('render', this.city, this.unit)
  }
}


